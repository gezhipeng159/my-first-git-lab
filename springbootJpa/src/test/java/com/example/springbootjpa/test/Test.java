package com.example.springbootjpa.test;

import com.example.springbootjpa.com.gzp.dao.PaymentDao;
import com.example.springbootjpa.com.gzp.entity.Payment;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:application.properties")
public class Test {
    @Resource
    private PaymentDao dao;


    @org.junit.Test
    public void  test(){
        Optional<Payment> payment = dao.findById("4028f9817a1379ba017a1379bbba0000");
        Payment payment1 = payment.get();
        System.out.println(payment1);
    }

}
