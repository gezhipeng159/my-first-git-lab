package com.example.springbootjpa.com.gzp.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Data
//对应表名
@Entity(name = "payment")
public class Payment {

    @Id    //主键id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")//主键生成策略
    @Column(name = "id")//数据库字段名
    private String id;

    @Column(name = "serial")
    private String serial;


}
